extern crate image;
use rand::{self, thread_rng, Rng};

use image::{ImageBuffer, Rgb};

const WIDTH: u32 = 256;
const HEIGHT: u32 = 256;

fn gen_star_sprite() {
    // a default (black) image containing Rgb values
    let mut image = ImageBuffer::<Rgb<u8>>::new(WIDTH, HEIGHT);

	let r = thread_rng().gen_range(128, 255);

    // set a central pixel to white
    image.get_pixel_mut(5, 5).data = [r, r, r];

    // write it out to a file
    image.save("output.png").unwrap();
}