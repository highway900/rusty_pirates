use amethyst::ecs::{Component, DenseVecStorage, VecStorage};
use amethyst::core::cgmath::Vector3;
use amethyst::renderer::{SpriteRender};

#[derive(Clone, PartialEq)]
pub struct Velocity(pub Vector3<f32>);

impl Component for Velocity {
    type Storage = VecStorage<Self>;
}

#[derive(Clone)]
pub struct Star;

impl Component for Star {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Clone, PartialEq)]
pub struct StarSpawner {
	pub star_sprite: SpriteRender,
}

impl Component for StarSpawner {
    type Storage = DenseVecStorage<Self>;
}