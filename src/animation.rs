use amethyst::assets::{Handle, Loader};
use amethyst::prelude::*;
use amethyst::renderer::SpriteRender;
use amethyst::animation::{
    Animation, InterpolationFunction, Sampler, SpriteRenderChannel, SpriteRenderPrimitive,
};

pub fn ship_anim(
        world: &mut World,
        sprite_sheet_id: u64,
        indices: Vec<u64>,
        indices_sampler: Vec<f32>) -> Handle<Animation<SpriteRender>> {
    let sprite_indicies = indices
        .into_iter()
        .map(|n| SpriteRenderPrimitive::SpriteIndex(n as usize))
        .collect::<Vec<SpriteRenderPrimitive>>();

    let sprite_index_sampler = {
        Sampler {
            input: indices_sampler,
            function: InterpolationFunction::Step,
            output: sprite_indicies,
        }
    };

    let sprite_sheet_sampler = Sampler {
        input: vec![1.0],
        function: InterpolationFunction::Step,
        output: vec![SpriteRenderPrimitive::SpriteSheet(sprite_sheet_id)],
    };

    println!("sprite_index_sampler: {:?}", sprite_index_sampler);
    println!("sprite_sheet_sampler: {:?}", sprite_sheet_sampler);

    let loader = world.write_resource::<Loader>();
    let sampler_animation_handle =
        loader.load_from_data(sprite_index_sampler, (), &world.read_resource());
    let sprite_sheet_sampler_animation_handle =
        loader.load_from_data(sprite_sheet_sampler, (), &world.read_resource());

    let animation = Animation {
        nodes: vec![
            (
                0,
                SpriteRenderChannel::SpriteSheet,
                sprite_sheet_sampler_animation_handle,
            ),
            (
                0,
                SpriteRenderChannel::SpriteIndex,
                sampler_animation_handle,
            ),
        ],
    };
    loader.load_from_data(animation, (), &world.read_resource())
}
