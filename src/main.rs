extern crate amethyst;
extern crate rand;

mod pirates;
mod components;
mod systems;
mod animation;
mod sprite;
mod sprite_sheet_loader;
mod png_loader;

use amethyst::input::InputBundle;
use amethyst::core::frame_limiter::FrameRateLimitStrategy;
use amethyst::core::transform::TransformBundle;
use amethyst::prelude::*;
use amethyst::renderer::{
	DisplayConfig, Pipeline,
	RenderBundle, Stage, DrawSprite, ALPHA, ColorMask, SpriteRender};

use amethyst::animation::{
	AnimationBundle,
};

use amethyst::utils::application_root_dir;

use std::time::Duration;

use pirates::Pirates;
use pirates::{ShipAnimationId};

fn main() -> Result<(), amethyst::Error> {
	amethyst::start_logger(Default::default());
	let app_root = application_root_dir();
	
	let path = format!(
		"{}/resources/display_config.ron",
		env!("CARGO_MANIFEST_DIR")
	);
	
	let config = DisplayConfig::load(&path);

	let binding_path = format!(
		"{}/resources/bindings_config.ron",
		app_root
	);

	let input_bundle = InputBundle::<String, String>::new()
		.with_bindings_from_file(binding_path)?;

	let pipe = Pipeline::build().with_stage(
		Stage::with_backbuffer()
			.clear_target([0.0, 0.0, 0.0, 1.0], 1.0)
			.with_pass(DrawSprite::new().with_transparency(ColorMask::all(), ALPHA, None))
	);

	let game_data = GameDataBuilder::default()
		.with_bundle(AnimationBundle::<ShipAnimationId, SpriteRender>::new(
			"animation_control_system",
			"sampler_interpolation_system",
		))?
		.with_bundle(
			// Handles transformations of textures
			TransformBundle::new()
				.with_dep(&["animation_control_system", "sampler_interpolation_system"]),
		)?
		.with_bundle(RenderBundle::new(pipe, Some(config)).with_sprite_sheet_processor())?
		.with_bundle(input_bundle)?
		.with(systems::ShipSystem, "ship_system", &["input_system"])
		.with(systems::VelocitySystem, "velocity_system", &["input_system"])
		.with(systems::BGSystemSpawner{
				elapsed_time: 0., last_time: 0., spawn_time: 1.0
			},
			"bg_system_spawner",
			&["input_system"])
		.with(systems::ShipAnimationSystem, "ship_animation_system", &["input_system"]);

	let assets_dir = format!("{}/assets/", app_root);

	let mut game = Application::build(assets_dir, Pirates::new())?
		.with_frame_limit(
			FrameRateLimitStrategy::SleepAndYield(Duration::from_millis(2)),
			144)
		.build(game_data)?;

	game.run();

	Ok(())
}
