use amethyst::input::{is_close_requested, is_key_down};
use amethyst::core::cgmath::{Vector3, Matrix4, Point3, Transform as CgTransform};
use amethyst::core::transform::{GlobalTransform, Transform};
use amethyst::ecs::prelude::{Component, DenseVecStorage, Entity, World};
use amethyst::prelude::*;
use amethyst::assets::{Loader, AssetStorage, Handle};
use amethyst::renderer::{
	Camera,
	Projection,
	VirtualKeyCode,
	SpriteSheet,
	SpriteSheetSet,
	SpriteSheetHandle,
	SpriteRender,
	MaterialTextureSet,
	ScreenDimensions,
};

use amethyst::animation::{
	get_animation_set, Animation, EndControl, AnimationCommand,
};

use animation;
use components::{StarSpawner};
use png_loader;
use sprite::SpriteSheetDefinition;
use sprite_sheet_loader;

pub const ARENA_HEIGHT: f32 = 512.0;
pub const ARENA_WIDTH: f32 = 512.0;
pub const SHIP_HEIGHT: f32 = 63.0;
pub const SHIP_WIDTH: f32 = 63.0;
const SPRITESHEET_SIZE: (f32, f32) = (512.0, 256.0);

pub enum SpriteIdx {
	Ship = 0,
	Stars = 1,
}

#[derive(Debug, Clone)]
struct LoadedSpriteSheet {
	sprite_sheet_handle: SpriteSheetHandle,
	sprite_sheet_index: u64,
	sprite_count: usize,
	sprite_w: f32,
	sprite_h: f32,
}

pub struct Pirates {
	entities: Vec<Entity>,
	//loaded_sprite_sheet: Option<LoadedSpriteSheet>,
}

impl Pirates {
	pub fn new() -> Self {
		Pirates {
			entities: Vec::new(),
			//loaded_sprite_sheet: None,
		}
	}
}

impl<'a, 'b> SimpleState<'a, 'b> for Pirates {
	fn handle_event(&mut self, _data: StateData<GameData>, event: StateEvent) -> SimpleTrans<'a, 'b> {
		if let StateEvent::Window(event) = event {
			if is_close_requested(&event) || is_key_down(&event, VirtualKeyCode::Escape) {
				return Trans::Quit
			}
		}		
		Trans::None
	}

	fn update(&mut self, data: &mut StateData<GameData>) -> SimpleTrans<'a, 'b> {
		data.data.update(&data.world);
		Trans::None
	}

	fn on_start(&mut self, data: StateData<GameData>) {
		let StateData { world, .. } = data;
		
		// TODO: Fix LoadedSpriteSheet cos this would be a good idea
		// let (sprite_sheet_handle, sprite_sheet_index, sprite_count, sprite_w, sprite_h) =
		// 	load_sprite_sheet(99, String::from("texture/ship_spritesheet.png"), world);

		// self.loaded_sprite_sheet = Some(LoadedSpriteSheet {
		// 	sprite_sheet_handle,
		// 	sprite_sheet_index,
		// 	sprite_count,
		// 	sprite_w,
		// 	sprite_h,
		// });

		world.register::<Ship>();

		self.initialise_camera(world);
		self.add_bg_spawner(world);
		self.draw_sprites_animated(world);
	}
}

impl Pirates {
	fn add_bg_spawner(&mut self, world: &mut World) {
		// Create a background spawning entity that spawns star sprites
		// that travel with the player to give the effect of motion.

		let (width, height) = {
			let dim = world.read_resource::<ScreenDimensions>();
			(dim.width(), dim.height())
		};

		let mut common_transform = Transform::default();
		common_transform.translation = Vector3::new(width / 2., height / 2., 0.);

		let mut spawner_transform = Transform::default();
		spawner_transform.translation = Vector3::new(50., 0., 0.);

		CgTransform::<Point3<f32>>::concat_self(&mut spawner_transform, &common_transform);
		
		let ss = 256.0;
		let (sprite_sheet_handle, ..) =
			load_sprite_sheet(SpriteIdx::Stars as u64, String::from("texture/star_texture.png"), ss, ss, 2, 1, ss, ss, world);

		let sprite_render_stars = SpriteRender {
		    sprite_sheet: sprite_sheet_handle.clone(),
		    sprite_number: SpriteIdx::Stars as usize,
		    flip_horizontal: false,
		    flip_vertical: false,
		};

		let star_spawner = StarSpawner{star_sprite: sprite_render_stars};

		let entity = world
			.create_entity()
			.with(spawner_transform)
			.with(star_spawner)
			.build();
		
		self.entities.push(entity);
	}

	fn draw_sprites_animated(&mut self, world: &mut World) {
		let (width, height) = {
			let dim = world.read_resource::<ScreenDimensions>();
			(dim.width(), dim.height())
		};
		// This `Transform` moves the sprites to the middle of the window
		let mut common_transform = Transform::default();
		common_transform.translation = Vector3::new(width / 2., height / 2., 0.);

		let (sprite_sheet_handle, sprite_sheet_index, sprite_h, ..) =
			load_sprite_sheet(
				SpriteIdx::Ship as u64,
				String::from("texture/ship_spritesheet.png"),
				SHIP_WIDTH,
				SHIP_HEIGHT,
				3,
				8,
				SPRITESHEET_SIZE.0,
				SPRITESHEET_SIZE.1,
				world);

		let mut sprite_transform = Transform::default();
		sprite_transform.translation = Vector3::new(0., sprite_h as f32 * 2.5, 0.);

		CgTransform::<Point3<f32>>::concat_self(&mut sprite_transform, &common_transform);

		let sprite_render = SpriteRender {
			sprite_sheet: sprite_sheet_handle.clone(),
			sprite_number: SpriteIdx::Ship as usize,
			flip_horizontal: false,
			flip_vertical: false,
		};

		let mut ship = Ship::new(Side::Good);
		ship.fly_animation_handle = Some(animation::ship_anim(
			world, sprite_sheet_index, vec![16, 17, 18, 19], vec![0., 0.1, 0.3, 0.5]));
		ship.fly_animation_handle_left = Some(animation::ship_anim(
			world, sprite_sheet_index, vec![1], vec![0.1]));

		let idle_handle_clone = &ship.fly_animation_handle.clone().unwrap();

		let player = world
			.create_entity()
			.with(ship)
			// Render info of the default sprite
			.with(sprite_render)
			// Shift sprite to some part of the window
			.with(sprite_transform)
			// Used by the engine to compute and store the rendered position.
			.build();

		// Initialise with the Idle animation
		let mut animation_control_set_storage = world.write_storage();
		let anim_set = get_animation_set::<ShipAnimationId, SpriteRender>(&mut animation_control_set_storage, player).unwrap();
		anim_set.add_animation(
			ShipAnimationId::Idle,
			idle_handle_clone,
			EndControl::Loop(None),
			1., // Rate at which the animation plays
			AnimationCommand::Start,
		);

		self.entities.push(player);
	}

	
	fn initialise_camera(&mut self, world: &mut World) -> Entity {
		// Position the camera. Here we translate it forward (out of the screen) far enough to view
		// all of the sprites. Note that camera_z is 12.0, whereas the furthest sprite is 11.0.
		//
		// For the depth, the additional + 1.0 is needed because the camera can see up to, but
		// excluding, entities with a Z coordinate that is `camera_z - camera_depth_vision`. The
		// additional distance means the camera can see up to just before -1.0 on the Z axis, so
		// we can view the sprite at 0.0.

		let (width, height) = {
			let dim = world.read_resource::<ScreenDimensions>();
			(dim.width(), dim.height())
		};

		world
			.create_entity()
			.with(Camera::from(Projection::orthographic(
				0.0, width, height, 0.0,
			))).with(GlobalTransform(Matrix4::from_translation(
				Vector3::new(0.0, 0.0, 1.0).into(),
			))).build()
	}
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum ShipAnimationId {
	Idle,
	Left,
}

#[derive(PartialEq, Eq, Clone, Hash)]
pub enum Side {
	Good,
}

#[derive(PartialEq, Clone)]
pub struct Ship {
	pub side: Side,
	pub width: f32,
	pub height: f32,
	pub fly_animation_handle: Option<Handle<Animation<SpriteRender>>>,
	pub fly_animation_handle_left: Option<Handle<Animation<SpriteRender>>>,
	pub is_turning: bool,
}

impl Ship {
	fn new(side: Side) -> Ship {
		Ship {
			side: side,
			width: 1.0,
			height: 1.0,
			fly_animation_handle: None,
			fly_animation_handle_left: None,
			is_turning: false,
		}
	}
}

impl<'a> Component for Ship {
	type Storage = DenseVecStorage<Self>;
}

fn load_sprite_sheet(
		idx: u64,
		filename: String,
		sprite_w: f32,
		sprite_h: f32,
		n_sprites_x: usize,
		n_sprites_y: usize,
		sheetsize_w: f32,
		sheetsize_h: f32,
		world: &mut World
	) -> (SpriteSheetHandle, u64, usize, f32, f32) {
	
	let sprite_sheet_index = idx;

	// Load the sprite sheet necessary to render the graphics
	// The texture is the pixel data
	// `texture_handle` is a clonable reference to the texture
	let texture_handle = png_loader::load(filename, world);

	// `texture_id` is a application defined ID given to the texture to store in
	// the `World`. This is needed to link the texture to the sprite_sheet.
	let texture_id = idx;
	world.write_resource::<MaterialTextureSet>()
		.insert(texture_id, texture_handle);
	
	// Create the sprite for the ships.
	//
	// Texture coordinates are expressed as a proportion of the sprite sheet's
	// dimensions between 0.0 and 1.0, so they must be divided by the width or
	// height.
	//
	// In addition, on the Y axis, texture coordinates are 0.0 at the bottom of
	// the sprite sheet and 1.0 at the top, which is the opposite direction of
	// pixel coordinates, so we have to invert the value by subtracting the
	// pixel proportion from 1.0.
	
	let sprite_sheet_definition = SpriteSheetDefinition::new(
		sprite_w, sprite_h, n_sprites_x, n_sprites_y, false, sheetsize_w, sheetsize_h);

	let sprite_sheet = sprite_sheet_loader::load(sprite_sheet_index, &sprite_sheet_definition);
	let sprite_count = 1;

	let sprite_sheet_handle = {
		let loader = world.read_resource::<Loader>();
		loader.load_from_data(
			sprite_sheet,
			(),
			&world.read_resource::<AssetStorage<SpriteSheet>>(),
		)
	};

	// Store a reference to the sprite sheet, necessary for sprite render animation
	world
		.write_resource::<SpriteSheetSet>()
		.insert(sprite_sheet_index, sprite_sheet_handle.clone());
	(
		sprite_sheet_handle,
		sprite_sheet_index,
		sprite_count,
		sprite_w,
		sprite_h,
	)
}