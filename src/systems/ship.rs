use amethyst::renderer::SpriteRender;
use amethyst::animation::{EndControl, AnimationCommand, AnimationControlSet};
use amethyst::core::Transform;
use amethyst::ecs::{Join, Read, ReadStorage, System, WriteStorage};
use amethyst::input::InputHandler;

use pirates::{Ship, Side, ShipAnimationId, SHIP_HEIGHT, SHIP_WIDTH, ARENA_HEIGHT, ARENA_WIDTH};

const SPEED: f32 = 1.7;

pub struct ShipSystem;

impl<'s> System<'s> for ShipSystem {
	type SystemData = (
		WriteStorage<'s, Transform>,
		WriteStorage<'s, Ship>,
		WriteStorage<'s, SpriteRender>,
		Read<'s, InputHandler<String, String>>,
	);

	fn run(&mut self, (mut transforms, mut ships, mut srs, input): Self::SystemData) {
		for (ship, transform, sr) in (&mut ships, &mut transforms, &mut srs).join() {

			let x = input.axis_value("X");
			let y = input.axis_value("Y");


			if ship.side == Side::Good {
				ship.is_turning = false;

				if let Some(mv_amount_x) = x {
					if mv_amount_x != 0.0 {
						ship.is_turning = false;
						let scaled_amount = SPEED * mv_amount_x as f32;

						transform.translation[1] = (transform.translation[1] + scaled_amount)
					  		.min(ARENA_WIDTH - SHIP_WIDTH * 0.5)
					  		.max(SHIP_WIDTH * 0.5);

						println!("moving {:?} {:?}", x, transform.translation[1]);
					}
				}

				if let Some(mv_amount_y) = y {
					if mv_amount_y != 0.0 {
						ship.is_turning = true;

						if y < Some(0.0) {
							sr.flip_horizontal = true;
						} else {
							sr.flip_horizontal = false;
						}
						
						
						let scaled_amount = SPEED * -mv_amount_y as f32;

						transform.translation[0] = (transform.translation[0] + scaled_amount)
							.min(ARENA_HEIGHT - SHIP_HEIGHT * 0.5)
							.max(SHIP_HEIGHT * 0.5);

						println!("moving {:?} {:?}", y, transform.translation[0]);
					}
				}
			}
		}
	}
}

pub struct ShipAnimationSystem;

impl<'s> System<'s> for ShipAnimationSystem {
	type SystemData = (
		ReadStorage<'s, Ship>,
		WriteStorage<'s, AnimationControlSet<ShipAnimationId, SpriteRender>>,
	);

	fn run(&mut self, (ships, mut anim_sets): Self::SystemData) {
		for (ship, anim_set) in (&ships, &mut anim_sets).join() {
			match ship.is_turning {
				false => {
					anim_set.abort(ShipAnimationId::Left);
					anim_set.add_animation(
						ShipAnimationId::Idle,
						&ship.fly_animation_handle.as_ref().unwrap(),
						EndControl::Loop(None),
						1., // Rate at which the animation plays
						AnimationCommand::Start,
					);
					// println!("{:?}", anim_set);
				}
				true => {
					anim_set.abort(ShipAnimationId::Idle);
					anim_set.add_animation(
						ShipAnimationId::Left,
						&ship.fly_animation_handle_left.as_ref().unwrap(),
						EndControl::Loop(None),
						1., // Rate at which the animation plays
						AnimationCommand::Start,
					);
					// println!("{:?}", anim_set);
				}
			}
		}
	}
}