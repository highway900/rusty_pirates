use rand::{thread_rng, Rng};

use amethyst::core::{GlobalTransform, Transform, Time};
use amethyst::core::specs::{Join, Read, System, Entities, WriteStorage, ReadStorage};

use amethyst::core::cgmath::{Vector3};
use amethyst::renderer::SpriteRender;

use components::{Velocity, StarSpawner, Star};


pub struct BGSystemSpawner {
	pub elapsed_time: f32,
	pub last_time: f32,
	pub spawn_time: f32,
}

impl<'s> System<'s> for BGSystemSpawner {
	type SystemData = (
		Read<'s, Time>,
		Entities<'s>,
		WriteStorage<'s, StarSpawner>,
		WriteStorage<'s, Transform>,
		WriteStorage<'s, GlobalTransform>,
		WriteStorage<'s, Velocity>,
		WriteStorage<'s, SpriteRender>,
		WriteStorage<'s, Star>,
	);

	// entities, mut stars, mut transforms
	fn run(&mut self, (time, entities, mut spawners, mut transforms, mut global_transforms, mut vels, mut srs, mut stars): Self::SystemData) {
		for spawner in (&mut spawners).join() {
			// Create a new sprite every x secs and take into account the velocity of 
			// the player
			if self.elapsed_time > self.spawn_time {
				let r_velocity = thread_rng().gen_range(-68.0, -47.0);
				let r_scale = thread_rng().gen_range(1.5, 3.5);
				let r_x = thread_rng().gen_range(300.0, 1200.0);
				// let rotation = Quaternion::from_arc(
				// 	Vector3::new(0.0, 1.0, 0.0),
				// 	Vector3::new(1.0, 0.0, 0.0),
				// 	None);
				//println!("{:?}", rot_z);
				//transform.rotation = rotation;

				let mut transform = Transform::default();
				transform.translation = Vector3::new(r_x, 1500., 0.0);
				transform.scale = Vector3::new(r_scale, r_scale, 0.0);

				let mut sprite = spawner.star_sprite.clone();
				sprite.flip_vertical = thread_rng().gen();
				sprite.flip_horizontal = thread_rng().gen();

				entities.build_entity()
					.with(GlobalTransform::new(), &mut global_transforms)
					.with(transform.clone(), &mut transforms)
					.with(sprite, &mut srs)
					.with(Star, &mut stars)
					.with(Velocity(Vector3::new(0., r_velocity, 0.)), &mut vels)
					.build();

				println!("BG Spawner spawn time: {:?}", self.elapsed_time);
				self.elapsed_time = 0.;

			} else {
				self.elapsed_time += time.delta_seconds();
			}
		}
	}
}


pub struct VelocitySystem;

impl<'s> System<'s> for VelocitySystem {
	type SystemData = (
		Entities<'s>,
		Read<'s, Time>,
		WriteStorage<'s, Transform>,
		ReadStorage<'s, Velocity>,
	);

	fn run(&mut self, (entities, time, mut transforms, vels): Self::SystemData) {
		for (transform, vel, entity) in (&mut transforms, &vels, &*entities).join() {
			let t = time.delta_seconds();
			transform.translation += vel.0 * t;
			println!("vel translation: {:?}", transform.translation);
			if transform.translation[1] < 0.0 {
				println!("Destorying Entity: {:?}", entity);
				let _ = entities.delete(entity);
			}
		}
	}
}